#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"
#include <sstream>


using namespace std;

int main(int argc, char ** argv) {

   // Criação de objetos com alocação estática
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);

   // Criação de objetos com alocação dinâmica
   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);   
  
   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   
   // Impressão dos atributos dos objetos utilizando método da própria classe
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();

   // Criação de objeto da classe filha
   Aluno aluno_1;
   aluno_1.setNome("João");
   cout << "Curso do aluno " << aluno_1.getNome() << ": " << aluno_1.getCurso() << endl;
   
   Professor *lprof[3];
   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
   string formacao;
   float salario;
   string sala;   
   string sida;
   string ssal;
   int i=0;
   
  cout << "insira os dados do professor"<< endl;

  for(i=0;i<3;i++)
  { 
   cout << "Nome: ";
   getline(cin,nome);
   cout << "Formacao: ";
   getline(cin,formacao);
   cout<< "Salario: ";
   getline(cin,ssal);
   stringstream(ssal)>> salario;
   cout << "Sala: ";
   getline(cin,sala);
   cout << "Matricula: ";
   getline(cin,matricula);
   cout << "Idade: ";
   getline (cin,sida);
   stringstream(sida) >> idade;
   cout << "Telefone: ";
   getline(cin,telefone);
   cout << "Sexo: ";
   getline(cin,sexo);
   
   lprof[i]=new Professor();
   
   lprof[i]->setNome(nome);
   lprof[i]->setFormacao(formacao);
   lprof[i]->setSalario(salario);
   lprof[i]->setSala(sala);
   lprof[i]->setMatricula(matricula);
   lprof[i]->setIdade(idade);
   lprof[i]->setTelefone(telefone);
   lprof[i]->setSexo(sexo);
  }
  
   // Exemplo de cadastro de uma lista de Alunos
   Aluno *lista_de_alunos[3]; // Criação da lista de tamanho fixo de ponteiros do objeto Aluno 

   
   float ira;
   int semestre;
   string curso;
   cout<< "insira os dados do aluno"<<endl;
   for(i=0;i<3;i++)
   {
   // Entrada de dados do terminal (stdin)
   cout << "Nome: ";
   getline(cin,nome);
   cout << "Matricula: ";
   getline(cin,matricula);
   cout << "Idade: ";
   getline (cin,sida);
   stringstream(sida) >> idade;

   // Criação do primeiro objeto da lista
   lista_de_alunos[i] = new Aluno();

   // Definição dos atributos do objeto
   lista_de_alunos[i]->setNome(nome);
   lista_de_alunos[i]->setMatricula(matricula);
   lista_de_alunos[i]->setIdade(idade);
   }
   for(i=0;i<3;i++){
   // Impressão dos atributos do objeto
     lista_de_alunos[i]->imprimeDadosAluno();
   }
   for(i=0;i<3;i++)
  {
   lprof[i]->imprimeDadosProfessor();
  }
   // Liberação de memória dos objetos criados dinamicamente
   for(i=0;i<3;i++)
   { 
     delete(lprof[i]);
     delete(lista_de_alunos[i]);
      
   }
   delete(pessoa_3);
   delete(pessoa_4);

   return 0;
}
