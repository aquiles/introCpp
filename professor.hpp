#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP
#include "pessoa.hpp"
#include <string>
using namespace std;

class Professor: public Pessoa
{
    private:
        string formacao;
        float salario;
        string sala;
    public:
        Professor();
        ~Professor();
        string getFormacao();
        float getSalario();
        string getSala();
        void setFormacao(string formacao );
        void setSalario(float salario);
        void setSala(string sala);
        void imprimeDadosProfessor();
};



#endif // PROFESSOR_HPP

